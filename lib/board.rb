class Board
  attr_reader :grid, :empty_grid, :empty_board
  def self.default_grid
    Array.new(10) { Array.new(10)}
  end

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def count
    @grid.flatten.count(:s)
  end

  def [](pos) #study up syntactic sugar
    x,y = [*pos]
    @grid[x][y] = :x
  end

  def []=(*pos,value) #study up syntactic sugar
    x,y = [*pos]
    @grid[x][y] = value
  end

  def empty?(coord=grid.count)
    return false if coord == 0
    x,y = coord[0],coord[1]
    @grid[x][y] != :s
  end

  def place_random_ship
     x= rand(self.grid.length)#setting x,y coord...
     y= rand(self.grid[0].length)
     if self.full? #didnt wanna mess with that >_<
       raise "Board is full."
     end
     self.grid[x][y]=:s
  end


  def full?
    @grid.flatten.all? {|coord| coord == :s || coord == :x}
  end

  def won?
      @grid.flatten.count(:s) == 0
  end

end
