class BattleshipGame
  attr_reader :board, :player, :two_ship_grid, :two_ship_board, :game, :get_play

  def initialize(player,board)
    @player = player
    @board = board
  end

  def attack(coord)

  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    #needed to fill out player classa bit to get player#get_play
    coord = @player.get_play
    self.attack(coord)
  end
end
